-----------------------------------------------------------------------------------------
--
-- stronger.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require("storyboard")
local scene = storyboard.newScene()

local function buttonTap(event)
    storyboard.gotoScene(event.target.destination, {effect = "fade"})
    return true
end
 
function scene:createScene(event)
    local group = self.view

    local title = display.newText("Força", 0, 0, "Helvetica", 80)
    title.x, title.y = centerX, topScreen + title.contentHeight
    group:insert(title)

    local backButton = display.newText("Voltar", 0, 0, "Helvetica", 50)
    backButton.x, backButton.y = leftScreen + backButton.contentWidth, heightScreen - backButton.contentHeight
    backButton.destination = "games" 
    backButton:addEventListener("tap", buttonTap)
    group:insert(backButton)
end

function scene:enterScene(event)
    local group = self.view
end

function scene:exitScene(event)
    local group = self.view
end

function scene:destroyScene(event)
    local group = self.view
end

scene:addEventListener("createScene", scene)
scene:addEventListener("enterScene", scene)
scene:addEventListener("exitScene", scene)
scene:addEventListener("destroyScene", scene)

return scene