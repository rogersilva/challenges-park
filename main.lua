-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

centerX = display.contentWidth / 2
centerY = display.contentHeight / 2
widthScreen = display.contentWidth
heightScreen = display.contentHeight
topScreen = display.screenOriginY
leftScreen = display.screenOriginX 
countTicket = -1

local spritesRock = require("spritesRock")
sheet = graphics.newImageSheet("image/spritesRock.png", spritesRock.sheetData)

sequenceRock = {
{ name="observing",
  frames= { 1, 2 },
  time = 3000,
  loopCount = 0 },
{ name="flinging",
  frames= { 3, 4, 5, 6, 7 },
  time = 2000,
  loopCount = 0 }
}

display.setStatusBar(display.HiddenStatusBar)

local sqlite3 = require("sqlite3")

local path = system.pathForFile("park.db", system.DocumentsDirectory)
local db = sqlite3.open(path)

print("version sqlite3 " .. sqlite3.version())

local function onSystemEvent(event)
	if (event.type == "applicationExit") then
		db:close()
	end
end

local createGames = [[CREATE TABLE IF NOT EXISTS games (id INTEGER PRIMARY KEY, tickets INTEGER);]]
local insertGames = [[INSERT INTO games(id, tickets) VALUES (1, 5);]]
local deleteGames = [[DELETE FROM games;]]

db:exec(createGames)

function fetchTicket()
	print("fetchTicket")
	for row in db:nrows("SELECT id, tickets FROM games WHERE id = 1;") do
		print("row tickets " .. row.tickets)
		countTicket = row.tickets;
	end

	if ((countTicket == nil) or (countTicket < 0)) then
		insertTicket()
	end
end

function insertTicket()
	print("insertTicket")
    db:exec(insertGames)
    fetchTicket()
end

function updateTicket(tickets)
	print("updateTicket " .. tickets)
	local update = "UPDATE games SET tickets = " .. tickets .. " WHERE id = 1;"
	db:exec(update)
end

function deleteTicket()
	print("deleteTicket")
	db:exec(deleteGames)
end

--deleteTicket()
fetchTicket()

local storyboard = require("storyboard")
storyboard.purgeOnSceneChange = true

storyboard.gotoScene("menu", { effect = "fade"})

Runtime:addEventListener("system", onSystemEvent)