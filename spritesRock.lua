local M = {}
M.sheetData = {
	frames = {
		{ name=Rock1, x = 260, y = 0, width = 52, height = 128, sourceX=1, sourceY=0, sourceWidth=53 , sourceHeight=128 },
		{ name=Rock2, x = 205, y = 0, width = 53, height = 128, sourceX=0, sourceY=0, sourceWidth=53 , sourceHeight=128 },
		{ name=Rock3, x = 144, y = 0, width = 59, height = 128, sourceX=0, sourceY=0, sourceWidth=59 , sourceHeight=128 },
		{ name=Rock4, x = 0, y = 130, width = 78, height = 123, sourceX=4, sourceY=2, sourceWidth=88 , sourceHeight=128 },
		{ name=Rock5, x = 0, y = 0, width = 81, height = 128, sourceX=1, sourceY=0, sourceWidth=84 , sourceHeight=128 },
		{ name=Rock6, x = 80, y = 130, width = 70, height = 120, sourceX=11, sourceY=2, sourceWidth=82 , sourceHeight=128 },
		{ name=Rock7, x = 83, y = 0, width = 59, height = 123, sourceX=2, sourceY=1, sourceWidth=63 , sourceHeight=128 }
	},
	sheetContentWidth = 512,
	sheetContentHeight = 256
}
return M