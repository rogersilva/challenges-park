-----------------------------------------------------------------------------------------
--
-- hitTarget.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require("storyboard")
local scene = storyboard.newScene()

local physics = require("physics")
physics.start()
physics.setGravity(0, 9.8)
--physics.setDrawMode("hybrid")

local hits = 0
local moves = 0
local boom = false

local function goToEndGame(result)
    local options = 
    { effect = "fade",
      time = 2000,
      params = 
      { win = result }
    }
    storyboard.gotoScene("gameOver", options)
    return true
end

local function ballTouched(event)
    if event.phase == "began" then 
        display.getCurrentStage():setFocus(event.target)  
    elseif event.phase == "ended" then
        moves = moves + 1
        physics.addBody(event.target, "dynamic", {bounce = 0.5, density = 0.5, friction = 0.5, radius = 15})
        event.target:applyLinearImpulse(((event.xStart - event.x) / 20), ((event.yStart - event.y) / 20), event.target.x, event.target.y)
        audio.play(soundThrow)
        display.getCurrentStage():setFocus(nil)
    end 
end

local function calculation()
    print("calculation")
    if (moves == 3) then
        display.remove(numberOne)
        display.remove(ball3)
        numberZero.x, numberZero.y = leftScreen + numberZero.contentWidth, topScreen + numberZero.contentHeight
        if (hits > 1) then
            if ((countTicket ~= nil) and (countTicket < 5)) then
                updateTicket(countTicket + 1)
                fetchTicket()
            end
            goToEndGame(true)
        else
            goToEndGame(false)
        end
    else
        if (moves == 1) then
            display.remove(numberThree)
            display.remove(ball1)
            numberTwo.x, numberTwo.y = leftScreen + numberTwo.contentWidth, topScreen + numberTwo.contentHeight
            ball2.x, ball2.y = leftScreen + 200, heightScreen - 100
        elseif (moves == 2) then
            display.remove(numberTwo)
            display.remove(ball2)
            numberOne.x, numberOne.y = leftScreen + numberOne.contentWidth, topScreen + numberOne.contentHeight
            ball3.x, ball3.y = leftScreen + 200, heightScreen - 100
        end
    end
    boom = false
end

local function groundColision(event)
    print("groundColision")
    if (boom == false) then
        local tm = timer.performWithDelay(1000, calculation)
    end
    boom = true
end

local function targetColision(event)
    print("targetColision")
    if (boom == false) then
        hits = hits + 1
        if (hits == 1) then
            star1.x, star1.y = widthScreen - star1.contentWidth, topScreen + star1.contentHeight
        elseif (hits == 2) then
            star2.x, star2.y = widthScreen - ((star2.contentWidth * 2) + 10), topScreen + star2.contentHeight
        elseif (hits == 3) then
            star3.x, star3.y = widthScreen - ((star3.contentWidth * 3) + 20), topScreen + star3.contentHeight
        end
        local tm = timer.performWithDelay(1000, calculation)
    end
    boom = true
end

function scene:createScene(event)
    local group = self.view

    local background = display.newImage("image/backgroundHitTarget.png")
    background.x, background.y = centerX, centerY
    group:insert(background)

    numberThree = display.newImage("image/three.png")
    numberThree.x, numberThree.y = leftScreen + numberThree.contentWidth, topScreen + numberThree.contentHeight
    group:insert(numberThree)

    numberTwo = display.newImage("image/two.png")
    numberTwo.x, numberTwo.y = leftScreen - numberTwo.contentWidth, topScreen - numberTwo.contentHeight
    group:insert(numberTwo)

    numberOne = display.newImage("image/one.png")
    numberOne.x, numberOne.y = leftScreen - numberOne.contentWidth, topScreen - numberOne.contentHeight
    group:insert(numberOne)

    numberZero = display.newImage("image/zero.png")
    numberZero.x, numberZero.y = leftScreen - numberZero.contentWidth, topScreen - numberZero.contentHeight
    group:insert(numberZero)

    local baseball = display.newImage("image/baseball.png")
    baseball.x, baseball.y = leftScreen + (baseball.contentWidth * 4), topScreen + (baseball.contentHeight * 2)
    group:insert(baseball)

    --local rock = display.newSprite(sheet, sequenceRock)
    --rock:setSequence("flinging")
    --rock:play()
    --rock.myName = "Rock"
    --rock.x, rock.y = leftScreen + 150, heightScreen - 60
    --group:insert(rock)

    target = display.newImage("image/target.png")
    target.x, target.y = widthScreen - (target.contentWidth / 2), heightScreen - (target.contentHeight / 2)
    group:insert(target)

    ball1 = display.newImage("image/baseball.png")
    ball1.x, ball1.y = leftScreen + 200, heightScreen - 100
    group:insert(ball1)

    ball2 = display.newImage("image/baseball.png")
    ball2.x, ball2.y = leftScreen - ball2.contentWidth, topScreen - ball2.contentHeight
    group:insert(ball2)

    ball3 = display.newImage("image/baseball.png")
    ball3.x, ball3.y = leftScreen - ball3.contentWidth, topScreen - ball3.contentHeight
    group:insert(ball3)

    star1 = display.newImage("image/star.png")
    star1.x, star1.y = leftScreen - star1.contentWidth, topScreen - star1.contentHeight
    group:insert(star1)

    star2 = display.newImage("image/star.png")
    star2.x, star2.y = leftScreen - star2.contentWidth, topScreen - star2.contentHeight
    group:insert(star2)

    star3 = display.newImage("image/star.png")
    star3.x, star3.y = leftScreen - star3.contentWidth, topScreen - star3.contentHeight
    group:insert(star3)

    leftWall = display.newRect(leftScreen, centerY, 1, heightScreen)
    leftWall:setFillColor(0, 0, 0, 0)
    group:insert(leftWall)

    rightWall = display.newRect(widthScreen, centerY, 1, heightScreen)
    rightWall:setFillColor(0, 0, 0, 0)
    group:insert(rightWall)

    sky = display.newRect(centerX, topScreen, widthScreen, 1)
    sky:setFillColor(0, 0, 0, 0)
    group:insert(sky)

    ground = display.newRect(centerX, heightScreen, widthScreen, 1)
    ground:setFillColor(0, 0, 0, 0)
    group:insert(ground)

    soundThrow = audio.loadSound("sound/throw.mp3")
end

function scene:enterScene(event)
    local group = self.view

    if ((countTicket ~= nil) and (countTicket > 0)) then
        updateTicket(countTicket - 1)
        fetchTicket()
    end

    hits, moves = 0, 0
    boom = false

    physics.addBody(target, "static", {bounce = 0.5, density = 1.0, friction = 0.3, radius = 30})

    physics.addBody(leftWall, "static", {bounce = 0.5, density = 1.0, friction = 0.3})
    physics.addBody(rightWall, "static", {bounce = 0.5, density = 1.0, friction = 0.3})
    physics.addBody(sky, "static", {bounce = 0.5, density = 1.0, friction = 0.1})
    physics.addBody(ground, "static", {bounce = 0.5, density = 1.0, friction = 0.6})

    ball1:addEventListener("touch", ballTouched)
    ball2:addEventListener("touch", ballTouched)
    ball3:addEventListener("touch", ballTouched)

    ground:addEventListener('collision', groundColision)
    target:addEventListener('collision', targetColision)

    --leftWall:addEventListener('collision', groundColision)
    --leftWall:addEventListener('collision', groundColision)
    --sky:addEventListener('collision', groundColision)
end

function scene:exitScene(event)
    local group = self.view
end

function scene:destroyScene(event)
    local group = self.view
end

scene:addEventListener("createScene", scene)
scene:addEventListener("enterScene", scene)
scene:addEventListener("exitScene", scene)
scene:addEventListener("destroyScene", scene)

return scene