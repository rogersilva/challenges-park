-----------------------------------------------------------------------------------------
--
-- games.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require("storyboard")
local scene = storyboard.newScene()

local function buttonTap(event)
    storyboard.gotoScene(event.target.destination, {effect = "fade"})
    return true
end

function scene:createScene(event)
    local group = self.view

    local background = display.newImage("image/backgroundGames.png")
    background.x, background.y = centerX, centerY
    group:insert(background)

    local title = display.newImage("image/games.png")
    title.x, title.y = centerX, topScreen + (title.contentHeight * 1.5)
    group:insert(title)

    if (countTicket ~= nil) then
        if (countTicket == 5) then
            local number = display.newImage("image/five.png")
            number.x, number.y = widthScreen - number.contentWidth, topScreen + number.contentHeight
            group:insert(number)
        elseif (countTicket == 4) then
            local number = display.newImage("image/four.png")
            number.x, number.y = widthScreen - number.contentWidth, topScreen + number.contentHeight
            group:insert(number)
        elseif (countTicket == 3) then
            local number = display.newImage("image/three.png")
            number.x, number.y = widthScreen - number.contentWidth, topScreen + number.contentHeight
            group:insert(number)
        elseif (countTicket == 2) then
            local number = display.newImage("image/two.png")
            number.x, number.y = widthScreen - number.contentWidth, topScreen + number.contentHeight
            group:insert(number)
        elseif (countTicket == 1) then
            local number = display.newImage("image/one.png")
            number.x, number.y = widthScreen - number.contentWidth, topScreen + number.contentHeight
            group:insert(number)
        elseif (countTicket == 0) then
            local number = display.newImage("image/zero.png")
            number.x, number.y = widthScreen - number.contentWidth, topScreen + number.contentHeight
            group:insert(number)
        end
    end

    local ticket = display.newImage("image/ticket.png")
    ticket.x, ticket.y = widthScreen - (ticket.contentWidth * 2), topScreen + ticket.contentHeight
    group:insert(ticket)

    local backButton = display.newImage("image/buttonBack.png")
    backButton.x, backButton.y = leftScreen + backButton.contentWidth, topScreen + backButton.contentHeight
    backButton.destination = "menu" 
    backButton:addEventListener("tap", buttonTap)
    group:insert(backButton)

    local target = display.newImage("image/buttonHitTarget.png")
    target.x, target.y = centerX - target.contentWidth, centerY + (target.contentHeight / 6)
    target.destination = "hitTarget"
    target:addEventListener("tap", buttonTap)
    group:insert(target)

    --local stronger = display.newText("Força", 0, 0, "Helvetica", 80)
    --stronger.x, stronger.y = centerX + stronger.height * 2, centerY
    --stronger.destination = "stronger"
    --stronger:addEventListener("tap", buttonTap)
    --group:insert(stronger)

    local rock = display.newSprite(sheet, sequenceRock)
    rock:setSequence("observing")
    rock:play()
    rock.myName = "Rock"
    rock.x, rock.y = leftScreen + 100, heightScreen - 80
    group:insert(rock)

    local obstaculo = display.newImage("image/ObstaculoSide.png")
    obstaculo.x, obstaculo.y = leftScreen + 200, heightScreen - 100
    group:insert(obstaculo)
end

function scene:enterScene(event)
    local group = self.view
end

function scene:exitScene(event)
    local group = self.view
end

function scene:destroyScene(event)
    local group = self.view
end

scene:addEventListener("createScene", scene)
scene:addEventListener("enterScene", scene)
scene:addEventListener("exitScene", scene)
scene:addEventListener("destroyScene", scene)

return scene