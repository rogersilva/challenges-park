-----------------------------------------------------------------------------------------
--
-- parkStore.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require("storyboard")
local scene = storyboard.newScene()

local function buttonTap(event)
    storyboard.gotoScene(event.target.destination, {effect = "fade"})
    return true
end

function scene:createScene(event)
    local group = self.view

    local background = display.newImage("image/backgroundStore.png")
    background.x, background.y = centerX, centerY
    group:insert(background)

    local title = display.newImage("image/store.png")
    title.x, title.y = centerX, topScreen + title.contentHeight * 2
    group:insert(title)

    local backButton = display.newImage("image/buttonBack.png")
    backButton.x, backButton.y = leftScreen + backButton.contentWidth, topScreen + backButton.contentHeight
    backButton.destination = "menu" 
    backButton:addEventListener("tap", buttonTap)
    group:insert(backButton)
end

function scene:enterScene(event)
    local group = self.view
end

function scene:exitScene(event)
    local group = self.view
end

function scene:destroyScene(event)
    local group = self.view
end

scene:addEventListener("createScene", scene)
scene:addEventListener("enterScene", scene)
scene:addEventListener("exitScene", scene)
scene:addEventListener("destroyScene", scene)

return scene