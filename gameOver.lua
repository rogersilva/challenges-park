-----------------------------------------------------------------------------------------
--
-- gameOver.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require("storyboard")
local scene = storyboard.newScene()

local function buttonTap(event)
    storyboard.gotoScene(event.target.destination, {effect = "fade"})
    return true
end

function scene:createScene(event)
    local group = self.view

    local params = event.params

    local background = display.newImage("image/backgroundGameOver.png")
    background.x, background.y = centerX, centerY
    group:insert(background)

    if (params.win ~= nil) then
        if (params.win) then
            local title = display.newImage("image/youWin.png")
            title.x, title.y = centerX, topScreen + (title.contentHeight * 2)
            group:insert(title)
        else
            local title = display.newImage("image/youLose.png")
            title.x, title.y = centerX, topScreen + (title.contentHeight * 2)
            group:insert(title)
        end
    end

    local backButton = display.newImage("image/buttonBack.png")
    backButton.x, backButton.y = leftScreen + backButton.contentWidth, topScreen + backButton.contentHeight
    backButton.destination = "games" 
    backButton:addEventListener("tap", buttonTap)
    group:insert(backButton)

    local againButton =display.newImage("image/buttonAgain.png")
    againButton.x, againButton.y = widthScreen - againButton.contentWidth, topScreen + againButton.contentHeight
    againButton.destination = "hitTarget" 
    againButton:addEventListener("tap", buttonTap)
    group:insert(againButton)
end

function scene:enterScene(event)
    local group = self.view
end

function scene:exitScene(event)
    local group = self.view
end

function scene:destroyScene(event)
    local group = self.view
end

scene:addEventListener("createScene", scene)
scene:addEventListener("enterScene", scene)
scene:addEventListener("exitScene", scene)
scene:addEventListener("destroyScene", scene)

return scene