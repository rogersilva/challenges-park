application = {

    showRuntimeErrors = true,
    launchPad = false,

	content = {
		width = 640,
		height = 960, 
		scale = "letterBox",
		fps = 60,
        xAlign = "left",
        yAlign = "top",
        shaderPrecision = "highp",

        imageSuffix = {
            ["-small"] = 0.375,
            ["@2x"] = 1.5
        }
	},

    license = {
        google = {
            key = "Your key here",
            policy = "this is optional",
        },
    },

    --[[
    -- Push notifications

    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert", "newsstand"
            }
        }
    }
    --]]
}
