-----------------------------------------------------------------------------------------
--
-- menu.lua
--
-----------------------------------------------------------------------------------------

local storyboard = require("storyboard")
local scene = storyboard.newScene()

local function buttonTap(event)
    storyboard.gotoScene(event.target.destination, {effect = "fade"})
end

local function flyBlack(object)
    object.x, object.y = widthScreen + object.width, heightScreen - object.height * 6
    transition.to(object, {time = 9000, delay = 3000, x = (leftScreen - 10), y = (topScreen - 20)})
end

local function flyPink(object)
    object.x, object.y = leftScreen - object.width * 2, heightScreen - object.height * 7
    transition.to(object, {time = 7000, delay = 2000, x = (widthScreen - 10), y = (topScreen - 20)})
end

function scene:createScene(event)
    local group = self.view

    local background = display.newImage("image/backgroundMenu.png")
    background.x, background.y = centerX, centerY
    group:insert(background)

    airplaneBlack = display.newImage("image/airplaneBlack.png")
    airplaneBlack.x, airplaneBlack.y = widthScreen + airplaneBlack.contentWidth, heightScreen - airplaneBlack.contentHeight * 6
    group:insert(airplaneBlack)

    airplanePink = display.newImage("image/airplanePink.png")
    airplanePink.x, airplanePink.y = leftScreen + airplanePink.contentWidth * 2, heightScreen - airplanePink.contentHeight * 7
    group:insert(airplanePink)

    balloon = display.newImage("image/balloonPink.png")
    balloon.x, balloon.y = widthScreen - balloon.contentWidth * 7, heightScreen - balloon.contentHeight * 6
    group:insert(balloon)

    local title = display.newImage("image/challengesPark.png")
    title.x, title.y = centerX, topScreen + title.contentHeight * 2
    group:insert(title)

    gamesButton = display.newImage("image/balloonMain.png")
    gamesButton.x, gamesButton.y = centerX, centerY
    gamesButton.destination = "games" 
    gamesButton:addEventListener("tap", buttonTap)
    group:insert(gamesButton)

    local settingsButton = display.newImage("image/buttonSettings.png")
    settingsButton.x, settingsButton.y = leftScreen + settingsButton.contentWidth, topScreen + settingsButton.contentHeight
    settingsButton.destination = "settings" 
    settingsButton:addEventListener("tap", buttonTap)
    group:insert(settingsButton)

    local storeButton = display.newImage("image/buttonStore.png")
    storeButton.x, storeButton.y = widthScreen - storeButton.contentWidth, topScreen + storeButton.contentHeight
    storeButton.destination = "parkStore" 
    storeButton:addEventListener("tap", buttonTap)
    group:insert(storeButton)

    soundSuspense = audio.loadSound("sound/suspense.mp3")
end

function scene:enterScene(event)
    local group = self.view

    transition.to(airplaneBlack, {time = 9000, delay = 1000, x = (leftScreen - 10), y = (topScreen - 20), onComplete = flyBlack})

    transition.to(airplanePink, {time = 7000, x = (widthScreen - 10), y = (topScreen - 20), onComplete = flyPink})

    transition.to(balloon, {time = 18000, x = (leftScreen - 10), y = (heightScreen - balloon.height * 6)})

    transition.to(gamesButton, {time = 4000, x = (centerX + 80), y = centerY})
    transition.to(gamesButton, {time = 8000, delay = 4000, x = (centerX - 80), y = centerY})
    transition.to(gamesButton, {time = 4000, delay = 12000, x = centerX, y = centerY})

    audio.play(soundSuspense)
end

function scene:exitScene(event)
    local group = self.view

    transition.pause(airplaneBlack)
    transition.pause(airplanePink)
    transition.pause(balloon)
    transition.pause(gamesButton)

    audio.pause(soundSuspense)
end

function scene:destroyScene(event)
    local group = self.view

    --audio.dispose(soundSuspense)
end

scene:addEventListener("createScene", scene)
scene:addEventListener("enterScene", scene)
scene:addEventListener("exitScene", scene)
scene:addEventListener("destroyScene", scene)

return scene